# README #

AUTO-FOLLOW-TWEETY-BOT is another version of my python bot , that follows all users from a textfile, which mean if you manage to get a huge list of users you will get tons of users followed and they ill follow you back.

### Requirements: ###

* Python 2.7
* Python lib : Coverage
* Python lib : Requests
* Python lib : Requests-coveralls
* Python lib : Nose-cov
* Python lib : Responses

* Go to the script folder and install using this command : easy_install twython  | or |  pip install twython 

### How to use: ###

* Example : scriptname.py textfile.txt
